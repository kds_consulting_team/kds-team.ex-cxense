FROM quay.io/keboola/docker-custom-python:latest
COPY . /code/

RUN pip install flake8
RUN pip3 install -r /code/requirements.txt

WORKDIR /code/
CMD ["python", "-u", "/code/src/main.py"]
