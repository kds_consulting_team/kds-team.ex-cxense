import csv
import io
import os

"""
Set of classes for standardized result processing, without use of Pandas library - more memory efficient.

Defines three classes 
- KBCResult - definition of a result that can be processed by other methods
- KBCTableDef - definition of a KBC Storage table - column, pkeys, name and other metadata
- ResultWriter - Class providing methods of generic parsing and writing JSON responses. Can be used to build more complex
 writer based on this class as a parent.

"""
class KBCResult:

    def __init__(self, file_name, full_path, table_def):
        self.full_path = full_path
        self.file_name = file_name
        self.table_def = table_def


class KBCTableDef:
    def __init__(self, name, columns, pk, destination='', metadata=None, column_metadata=None):
        self.pk = pk
        self.columns = columns
        self.name = name
        self.destination = destination
        self.metadata = metadata
        self.column_metadata = column_metadata


class ResultWriter:
    def __init__(self, result_dir_path: str, table_def: KBCTableDef, fix_headers=False,
                 exclude_fields=[], user_value_cols=[], flatten_objects=True, buffer_size=io.DEFAULT_BUFFER_SIZE):
        """

         :param write_header: asddas
         :param write_header: object
         :param table_def: KBCTableDef
         :param result_dir_path: str
         :param fix_headers: flag whether to use header specified in table definition or rather infer the header from
         the result itself. A Value Exception is thrown if no columns are specified in the table_def object and this
         option is set to True

         Note that with this option off all responses processed by the write() method must contain same header columns,
         otherwise invalid csv file is produced.

         :param flatten_objects: Specifies whether to flatten the parsed object or write them as are.
         If set to true all object hierarchies are flattened with `_` separator. Arrays are not parsed and stored as
         normal values.

         **Note:** If set to `True` and  used in combination with `fix_headers` field the columns specified include
         all flattened objects, hierarchy separated by `_` e.g. cols = ['single_col', 'parent_child'] otherwise these
         will not be included.

         :param exclude_fields: List of col names to be excluded from parsing.
         Usefull with `flatten_object` parameter on. This is applied before flattening.

         :param user_value_cols: List of column names that will be added to the root object. Use this in cocnlusion with
         `user_values` parameter in the write() method.


         :rtype: object
         """
        self.result_dir_path = result_dir_path
        self.table_def = table_def
        if user_value_cols:
            self.table_def.columns += user_value_cols
        self.results = {}
        self.fix_headers = fix_headers
        self.exclude_fields = exclude_fields
        self.flatten_objects = flatten_objects
        self.user_value_cols = user_value_cols

        # Cache open files, to limit expensive open operation count
        self._res_file_cache = {}
        self._buffer_size = buffer_size

    def write_all(self, data_array, file_name=None, user_values=None, object_from_arrays=False, write_header=True):

        for data in data_array:
            self.write(data, file_name=file_name, user_values=user_values, object_from_arrays=object_from_arrays,
                       write_header=write_header)

    def write(self, data, file_name=None, user_values=None, object_from_arrays=False, write_header=True):
        """

        :param data:
        :param file_name:
        :param user_values: Optional dictionary of user values to be added to the root object. The keys must match the column
        names specified during initialization in `user_value_cols` parameter, otherwise it will throw a ValueError.

        If used with `fix_headers` option, `user_value` columns must be specified on the table def object.

        :param object_from_arrays: Create additional tables from the array objects
        The result tables are named like: `parent_array-colnam.csv' and generates PK values: [parent_key, row_number]

        :param write_header: Optional flag specifying whether to write the header - only needed when creating sliced files.
        :return:
        """
        if not data:
            return {}

        if not file_name:
            file_name = self.table_def.name + '.csv'

        res_file_path = os.path.join(self.result_dir_path, file_name)

        # exclude columns
        data = self._exclude_cols(data)

        # flatten objects
        if self.flatten_objects:
            data = self.flatten_json(data)

        # add user values
        data = self._add_user_values(data, user_values)

        # write array objects
        if object_from_arrays:
            res = self._write_array_object(data, write_header, user_values)
            self.results = {**self.results, **res}

        fieldnames = self._get_header(data)
        # update columns in result if not specified
        if not self.fix_headers:
            self.table_def.columns = fieldnames

        self._write_data_to_csv(data, res_file_path, write_header, fieldnames)

        self.results[file_name] = (KBCResult(file_name, res_file_path, self.table_def))

    def collect_results(self):

        return [self.results[r] for r in self.results]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        """
        Close all output streams / files. Has to be called at end of extraction, before result processing.

        :return:
        """
        for res in self._res_file_cache:
            self._res_file_cache[res].close()

    def _write_array_object(self, data, write_header, user_values):
        results = {}
        for arr in [(key, data[key]) for key in data.keys() if isinstance(data[key], list)]:
            res = [{"row_nr": idx, "parent_key": self._get_pkey_values(data, user_values), "value": val} for idx, val in
                   enumerate(arr[1])]
            tb_name = self.table_def.name + '_' + arr[0]
            filename = tb_name + '.csv'
            res_path = os.path.join(self.result_dir_path, filename)
            columns = ["row_nr", "parent_key", "value"]
            self._write_data_to_csv(res, res_path, write_header, columns)
            res = KBCResult(filename, res_path,
                            KBCTableDef(tb_name, columns, ["row_nr", "parent_key"]))
            results[res.file_name] = res
            # remove field from source
            data.pop(arr[0])
        return results

    def _get_pkey_values(self, data, user_values):
        pkeys = []
        for k in self.table_def.pk:
            if data.get(k):
                pkeys.append(str(data[k]))
            elif user_values.get(k):
                pkeys.append(str(user_values[k]))

        return '|'.join(pkeys)

    def _write_data_to_csv(self, data, res_file_path, write_header, fieldnames):
        # append if exists
        if self._res_file_cache.get(res_file_path):
            res_file = self._res_file_cache.get(res_file_path)
            write_header = False
        else:
            res_file = open(res_file_path, 'w', encoding='utf-8', newline='', buffering=self._buffer_size)
            self._res_file_cache[res_file_path] = res_file

        writer = csv.DictWriter(res_file, fieldnames=fieldnames)
        if write_header:
            writer.writeheader()
        if isinstance(data, list):
            writer.writerows(data)
        else:
            writer.writerow(data)

    def _get_header(self, data):
        if self.fix_headers:
            cols = self.table_def.columns
        else:
            cols = list(data.keys())
        cols.sort()
        return cols

    def flatten_json(self, x, out={}, name=''):
        if type(x) is dict:
            for a in x:
                self.flatten_json(x[a], out, name + a + '.')
        else:
            out[name[:-1]] = x

        # self.flatten_json(y)
        return out

    def _exclude_cols(self, data):
        for col in self.exclude_fields:
            if col in data.keys():
                data.pop(col)
        return data

    def _add_user_values(self, data, user_values):
        if not user_values:
            return data

        # validate
        if len(user_values) > 0 and not all(elem in user_values.keys() for elem in self.user_value_cols):
            raise ValueError("Some user value keys (%s) were not set in user_value_cols (%s) during initialization!",
                             user_values.keys(), self.user_value_cols)

        data = {**data, **user_values}

        for key in user_values:
            data[key] = user_values[key]
        return data
