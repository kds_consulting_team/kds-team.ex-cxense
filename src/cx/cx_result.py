from kbc_lib.result import *

# from memory_profiler import profile
# always included
DMP_TRAFFIC_DATA_PK = ['eventId', 'userId']


class CxTrafficDataResultWriter(ResultWriter):
    """
    Stores dmp/traffic/data and traffic/data result data in CSV.

    """

    def __init__(self, out_path, endpoint_type, cols, buffer=8192):
        # specify result table
        event_res_table = KBCTableDef(endpoint_type + '_traffic-events', cols, DMP_TRAFFIC_DATA_PK)
        ResultWriter.__init__(self, out_path, event_res_table, fix_headers=True,
                              exclude_fields=['externalUserIds', 'customParameters', 'userParameters',
                                              'retargetingParameters'],
                              user_value_cols=['pk'], buffer_size=buffer)

        ext_user_table = KBCTableDef(endpoint_type + '_traffic-event-ext-users', ['id', 'type'], ['parent_pk', 'id'])
        self.ext_user_writer = ResultWriter(out_path, ext_user_table, fix_headers=True, flatten_objects=False,
                                            user_value_cols={'parent_pk'}, buffer_size=buffer)

        cust_par_tb = KBCTableDef(endpoint_type + '_traffic-event-custom-pars', ['item', 'group'],
                                  ['parent_pk', 'group', 'item'])
        self.cust_par_writer = ResultWriter(out_path, cust_par_tb, fix_headers=True,
                                            flatten_objects=False,
                                            user_value_cols=['parent_pk'], buffer_size=buffer)

        user_par_tb = KBCTableDef(endpoint_type + '_traffic-event-user-pars', ['item', 'group'],
                                  ['parent_pk', 'group', 'item'])
        self.user_par_writer = ResultWriter(out_path, user_par_tb, fix_headers=True,
                                            flatten_objects=False,
                                            user_value_cols=['parent_pk'], buffer_size=buffer)

        retarget_par_tb = KBCTableDef(endpoint_type + '_traffic-retarget-custom-pars', ['item', 'group'],
                                      ['parent_pk', 'group', 'item'])
        self.retarget_par_writer = ResultWriter(out_path, retarget_par_tb, fix_headers=True,
                                                flatten_objects=False,
                                                user_value_cols=['parent_pk'], buffer_size=buffer)

    # @profile
    def write(self, data, file_name=None, user_values=None, object_from_arrays=False, write_header=True):

        # write ext users
        ext_ids = data.get('externalUserIds')
        if ext_ids:
            self.ext_user_writer.write_all(ext_ids, object_from_arrays=False,
                                           user_values={'parent_pk': self._get_pkey_values(data, [])})
            self.results = {**self.results, **self.ext_user_writer.results}

        cust_pars = data.get('customParameters')
        if cust_pars:
            self.cust_par_writer.write_all(cust_pars, object_from_arrays=False,
                                           user_values={'parent_pk': self._get_pkey_values(data, [])})
            self.results = {**self.results, **self.cust_par_writer.results}

        user_pars = data.get('userParameters')
        if user_pars:
            self.user_par_writer.write_all(user_pars, object_from_arrays=False,
                                           user_values={'parent_pk': self._get_pkey_values(data, [])})
            self.results = {**self.results, **self.user_par_writer.results}

        retarget_pars = data.get('retargetingParameters')
        if retarget_pars:
            self.retarget_par_writer.write_all(retarget_pars, object_from_arrays=False,
                                               user_values={'parent_pk': self._get_pkey_values(data, [])})
            self.results = {**self.results, **self.retarget_par_writer.results}

        super().write(data, object_from_arrays=False, user_values={'pk': self._get_pkey_values(data, [])})


class CxTrafficResultWriter(ResultWriter):
    """
    Stores dmp/traffic/data and traffic/data result data in CSV.

    """

    def __init__(self, out_path, cols, history_cols, buffer=8192):
        # specify result table
        traffic_pk = cols + ['start', 'stop']
        traffic_res_table = KBCTableDef('dmp_traffic', cols, traffic_pk)
        ResultWriter.__init__(self, out_path, traffic_res_table, fix_headers=True,
                              user_value_cols=['start', 'stop', 'req', 'pk'], buffer_size=buffer)

        traff_hist_pk = history_cols + ['parent_pk', 'start_time', 'stop_time']
        traffic_history_tbl = KBCTableDef('dmp_traffic_history', [], traff_hist_pk)
        self.traffic_history_writer = ResultWriter(out_path, traffic_history_tbl, fix_headers=False,
                                                   flatten_objects=False, user_value_cols={'parent_pk'},
                                                   buffer_size=buffer)

    # @profile
    def write(self, data, file_name=None, user_values=None, object_from_arrays=False, write_header=True):

        # write ext users
        history = data.get('historyData')

        if history:
            hist_data = self._build_history_data(data.get('historyData'), data.get('history'))

            self.traffic_history_writer.write_all(hist_data, object_from_arrays=False,
                                                  user_values={'parent_pk': self._get_pkey_values(data, {})})
            self.results = {**self.results, **self.traffic_history_writer.results}
        user_values.update({'pk': self._get_pkey_values(data, {}), 'start': data['start'], 'stop': data['stop']})
        super().write(data['data'], object_from_arrays=False, user_values=user_values)

    def _build_history_data(self, hist_resp, history_dt_buckets):
        history_data = []
        # get size of historical data
        hist_size = len(next(iter(hist_resp.values())))
        history_fields = list(hist_resp.keys())
        history_fields.sort()
        dt_bucket_rows = self._build_history_bucket_rows(history_dt_buckets)
        # build result array
        for i in range(hist_size):
            row = {}
            # build row
            for k in history_fields:
                row.update({k: hist_resp[k][i]})
                row.update({'start_time': dt_bucket_rows[i][0],
                            'stop_time': dt_bucket_rows[i][1]})

            history_data.append(row)

        return history_data

    def _build_history_bucket_rows(self, h_buckets):
        bucket_rows = []
        for i in range(len(h_buckets) - 1):
            bucket_rows.append([h_buckets[i], h_buckets[i + 1]])
        return bucket_rows
