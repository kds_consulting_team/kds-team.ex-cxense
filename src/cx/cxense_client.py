import datetime
import hashlib
import hmac
import json

from kbc.client_base import HttpClientBase

DEFAULT_HOST = 'https://api.cxense.com'

ENDPOINT_DMP_TRAFFIC = '/dmp/traffic'
ENDPOINT_DMP_TRAFFIC_DATA = '/dmp/traffic/data'
ENDPOINT_DMP_TRAFFIC_DATA_DESC = '/dmp/traffic/data/describe'
ENDPOINT_TRAFFIC_DATA = '/traffic/data'
ENDPOINT_TRAFFIC_DATA_DESC = '/traffic/data/describe'

DEFAULT_COUNT_LIMIT = 100000


class CxenseClient(HttpClientBase):
    def __init__(self, username, api_secret, max_retries, retry_on, host=DEFAULT_HOST):
        HttpClientBase.__init__(self, host, max_retries=max_retries, status_forcelist=retry_on,
                                default_http_header={'Content-Type': 'application/json; charset=utf-8'})
        self._username = username
        self._api_secret = api_secret

    def _get_auth_header(self):
        date = datetime.datetime.utcnow().isoformat() + "Z"
        signature = hmac.new(self._api_secret.encode('utf-8'), date.encode('utf-8'),
                             digestmod=hashlib.sha256).hexdigest()
        auth_header = {
            "X-cXense-Authentication": "username=%s date=%s hmac-sha256-hex=%s" % (self._username, date, signature)}
        return auth_header

    def get_traffic_data_events_paged(self, type, **kwargs):
        """

        :param type: 'dmp' or 'standard'.
            'dmp' -> returns data from dmp/traffic/data endpoint
            'standard' -> returns data from traffic/data endpoint
        :type externalUserIdTypes - Array of String
        :type fields - Array of String
        :type filters - Array of Object
        :type origins - Array of String
        :type siteGroupIds - Array of String
        :type siteIds - Array of String
        :type start - Integer or String
        :type stop  - Integer or String

        More info https://wiki.cxense.com/pages/viewpage.action?pageId=29547369

        :return: Generator object to iterate through pages
        """

        if type == 'standard':
            endpoint = ENDPOINT_TRAFFIC_DATA
        elif type == 'dmp':
            endpoint = ENDPOINT_DMP_TRAFFIC_DATA
        else:
            raise ValueError('Traffic data enpoint type not supported must be in [standard, dmp]')

        return self._get_post_pages(endpoint, kwargs, DEFAULT_COUNT_LIMIT)

    def get_dmp_traffic(self, request, **kwargs):
        """

        :type externalUserIdTypes - Array of String
        :type fields - Array of String
        :type filters - Array of Object
        :type origins - Array of String
        :type siteGroupIds - Array of String
        :type siteIds - Array of String
        :type start - Integer or String
        :type stop  - Integer or String

        More info https://wiki.cxense.com/pages/viewpage.action?pageId=29547369

        :return: dist response
        """

        return self.post_request(ENDPOINT_DMP_TRAFFIC, request)

    def get_dmp_data_description(self):
        return self.post_request(ENDPOINT_DMP_TRAFFIC_DATA_DESC, {})

    def get_traffic_data_description(self):
        return self.post_request(ENDPOINT_TRAFFIC_DATA_DESC, {})

    def _get_post_pages(self, url_path, data, count):
        has_more = True

        while has_more:
            data['count'] = count
            req_response = self.post_request(url_path, data)

            if req_response.get('paginationStop', 0) != 0:
                has_more = True
                data['paginationStart'] = req_response['paginationStop']
            else:
                has_more = False

            yield req_response

    def post_request(self, url_path, body, query_params={}, **kwargs):
        auth_header = self._get_auth_header()

        headers = kwargs.pop('headers', {})
        headers.update(auth_header)
        headers["Content-Type"] = "application/json; charset=utf-8"

        url = self.base_url + url_path
        return self.post(url, data=json.dumps(body), params=query_params, headers=headers, **kwargs)

    def _get_request(self, url_path, query_params, **kwargs):
        auth_header = self._get_auth_header()

        headers = kwargs.pop('headers', {})
        headers.update(auth_header)
        headers["Content-Type"] = "application/json; charset=utf-8"

        url = self.base_url + url_path
        return self.get_raw(url, params=query_params, headers=headers, **kwargs)
