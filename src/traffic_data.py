import logging
from typing import List

from kbc.env_handler import KBCEnvHandler

from cx.cx_result import CxTrafficDataResultWriter
from cx.cx_result import CxTrafficResultWriter
from cx.cxense_client import CxenseClient
from kbc_lib.result import *

# from memory_profiler import profile

# configuration parameters
PAR_TRAFFIC_REQUEST_METHOD = 'traffic_request_method'
PAR_ORIGINS = 'origins'
PAR_EXT_USER_TYPES = 'externalUserIdTypes'
PAR_FIELDS = 'fields'
PAR_SITE_IDS = 'site_ids_filter'
PAR_SITE_GROUPS = 'traffic_request_groups'
PAR_FILTERS = 'traffic_filters'
PAR_DT_START = 'traffic_request_start'
PAR_DT_STOP = 'traffic_request_stop'
PAR_HISTORY_FIELDS = 'historyFields'
PAR_HISTORY_BUCKETS = 'historyBuckets'
PAR_HISTORY_RESOLUTION = 'historyResolution'

PAR_API_SECRET = '#request_secret'
PAR_USERNAME = 'request_username'

DMP_TRAFFIC_FIELDS = ['events', 'uniqueUsers']

# always included
DMP_TRAFFIC_DATA_PK = ['eventId', 'userId']

DMP_TRAFFIC_PK = []

MANDATORY_PARS = [PAR_USERNAME, PAR_API_SECRET]

MAX_RETRIES = 10
RETRY_ON = [429, 502, 503, 504]

ENDPOINT_DMP_TRAFFIC_DATA = '/dmp/traffic/data'
ENDPOINT_DMP_TRAFFIC = '/dmp/traffic'
ENDPOINT_TRAFFIC_DATA = '/traffic/data'


class TrafficDataExtractor:
    def __init__(self, env_handler: KBCEnvHandler, debug=False):
        self.env_handler = env_handler
        # override debug from config
        if env_handler.cfg_params.get('debug'):
            debug = True

        env_handler.set_default_logger('DEBUG' if debug else 'INFO')

        try:
            env_handler.validate_config(MANDATORY_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)
        self.cfg_params = env_handler.cfg_params
        # init client
        self.client = CxenseClient(self.cfg_params[PAR_USERNAME], self.cfg_params[PAR_API_SECRET], MAX_RETRIES,
                                   RETRY_ON)

    # @profile
    def run(self):

        req_method = self.cfg_params.get(PAR_TRAFFIC_REQUEST_METHOD)

        logging.info("Collecting %s results..", req_method)
        if req_method in [ENDPOINT_TRAFFIC_DATA, ENDPOINT_DMP_TRAFFIC_DATA]:
            results = self.get_traffic_event_data()
        elif req_method == ENDPOINT_DMP_TRAFFIC:
            results = self.get_traffic_data()

        logging.info('Storing results...')
        self.create_manifests(results)

    def get_traffic_event_data(self):
        """
        Get event data. dmp/traffic/data or traffic/data endpoints.

        :rtype: results
        """
        traffic_endpoint_type = ''
        if self.cfg_params.get(PAR_TRAFFIC_REQUEST_METHOD) == ENDPOINT_TRAFFIC_DATA:
            traffic_endpoint_type = 'standard'
        elif self.cfg_params.get(PAR_TRAFFIC_REQUEST_METHOD) == ENDPOINT_DMP_TRAFFIC_DATA:
            traffic_endpoint_type = 'dmp'
        # validate params
        params = self._validate_n_clean_traffic_params(traffic_endpoint_type)
        # build request
        request = self._build_traffic_data_request(params)
        logging.info("Request parameters: %s", request)

        with CxTrafficDataResultWriter(self.env_handler.tables_out_path, traffic_endpoint_type,
                                       request['fields'].copy(),
                                       buffer=8192) as event_writer:
            for res in self.client.get_traffic_data_events_paged(traffic_endpoint_type, **request):
                event_writer.write_all(res['events'], object_from_arrays=True)

            results = event_writer.collect_results()

        return results

    def get_traffic_data(self):
        params = self._validate_n_clean_dmp_traffic_params()

        # build partial request, most of the parameters shared with */traffic/data
        request = self._build_traffic_data_request(params)

        if params.get(PAR_HISTORY_FIELDS):
            request[PAR_HISTORY_FIELDS] = params[PAR_HISTORY_FIELDS]

        if params.get(PAR_HISTORY_BUCKETS):
            request[PAR_HISTORY_BUCKETS] = params[PAR_HISTORY_BUCKETS]

        if params.get(PAR_HISTORY_RESOLUTION):
            request[PAR_HISTORY_RESOLUTION] = params[PAR_HISTORY_RESOLUTION]

        logging.info("Request parameters: %s", request)

        traffic_res_table = KBCTableDef('dmp_traffic', params.get(PAR_FIELDS), DMP_TRAFFIC_PK)
        with CxTrafficResultWriter(self.env_handler.tables_out_path, request['fields'].copy(),
                                   request[PAR_HISTORY_FIELDS].copy(), buffer=8192) as event_writer:

            res = self.client.get_dmp_traffic(request)
            # remove unused params
            request.pop(PAR_FIELDS, '')
            request.pop(PAR_HISTORY_FIELDS, '')
            event_writer.write(res, user_values={'req': str(request)}, object_from_arrays=False)

            results = event_writer.collect_results()

        return results

    def _validate_n_clean_dmp_traffic_params(self):
        params = self.cfg_params
        fields = params.get(PAR_FIELDS, [])

        if fields:
            invalid_vals = [elem for elem in fields if elem not in DMP_TRAFFIC_FIELDS]
            if invalid_vals:
                logging.error('Some field values %s are unsupported. Valid values are %s', invalid_vals,
                              DMP_TRAFFIC_FIELDS)
                is_valid = False
            self.cfg_params[PAR_FIELDS] = list(set(fields))
        else:
            # exclude externalUserIds => sometimes does not work for all user types
            self.cfg_params[PAR_FIELDS] = DMP_TRAFFIC_FIELDS

        return self.cfg_params

    def _build_traffic_data_request(self, params):
        req = {}
        if params.get(PAR_DT_START):
            req['start'] = params[PAR_DT_START]
        if params.get(PAR_DT_STOP):
            req['stop'] = params[PAR_DT_STOP]

        if params.get(PAR_EXT_USER_TYPES):
            req[PAR_EXT_USER_TYPES] = params[PAR_EXT_USER_TYPES]

        if params.get(PAR_FILTERS):
            req['filters'] = params[PAR_FILTERS]

        if params.get(PAR_SITE_GROUPS):
            req['siteGroupIds'] = params[PAR_SITE_GROUPS]

        if params.get(PAR_ORIGINS):
            req[PAR_ORIGINS] = params[PAR_ORIGINS]

        if params.get(PAR_SITE_IDS):
            req['siteIds'] = params[PAR_SITE_IDS]

        req['fields'] = params[PAR_FIELDS]

        return req

    def _validate_n_clean_traffic_params(self, type):
        """
        Further param validation and cleanup
        :param type: 'dmp' or 'standard' type
        :return:
        """

        is_valid = True
        params = self.cfg_params
        if type == 'dmp':
            end_desc = self.client.get_dmp_data_description()
        else:
            end_desc = self.client.get_traffic_data_description()

        valid_types = end_desc['externalUserIdTypes']
        valid_fields = end_desc['fields']
        # fields (utype rule)
        fields = params.get(PAR_FIELDS, [])

        if fields:
            invalid_vals = [elem for elem in fields if elem not in valid_fields]
            if invalid_vals:
                logging.error('Some field values %s are unsupported. Valid values are %s', invalid_vals, valid_fields)
                is_valid = False
            self.cfg_params[PAR_FIELDS] = list(set(fields + DMP_TRAFFIC_DATA_PK))
        else:
            # exclude externalUserIds => sometimes does not work for all user types
            self.cfg_params[PAR_FIELDS] = [f for f in valid_fields if f != 'externalUserIds']

        # uvalidate ser types

        u_types = self.cfg_params.get(PAR_EXT_USER_TYPES, [])
        if u_types:
            invalid_vals = [elem for elem in u_types if elem not in valid_types]
            if invalid_vals:
                logging.error('Some externalUserIdType values %s are unsupported. Valid values are %s', invalid_vals,
                              valid_types)
                is_valid = False
        elif 'externalUserIds' in self.cfg_params[PAR_FIELDS]:
            self.cfg_params[PAR_EXT_USER_TYPES] = valid_types

        if any(['externalUserIds' in self.cfg_params[PAR_FIELDS],
                bool(self.cfg_params.get(PAR_EXT_USER_TYPES))]) and not (
                'externalUserIds' in self.cfg_params[PAR_FIELDS] and self.cfg_params.get(PAR_EXT_USER_TYPES)):
            logging.error(
                'Either both field "externalUserIds" and parameter "externalUserIdType" must be specified or none.')
            is_valid = False

        if not is_valid:
            exit(1)
        return self.cfg_params

    def create_manifests(self, results: List[KBCResult], headless=False, incremental=True):
        for r in results:
            if not headless:
                self.env_handler.configuration.write_table_manifest(r.full_path, r.table_def.destination,
                                                                    r.table_def.pk,
                                                                    None, incremental, r.table_def.metadata,
                                                                    r.table_def.column_metadata)
            else:
                self.env_handler.configuration.write_table_manifest(r.full_path, r.table_def.destination,
                                                                    r.table_def.pk,
                                                                    r.table_def.columns, incremental,
                                                                    r.table_def.metadata,
                                                                    r.table_def.column_metadata)
